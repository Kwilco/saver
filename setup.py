import setuptools


setuptools.setup(
    name='saver',
    version='0.1.0',
    description='Saves reddit saved items to disk.',
    author='Kyle Wilcox',
    author_email='k.j.wilcox@gmail.com',
    packages=setuptools.find_packages(),
    include_package_data=True,
    entry_points={
        'console_scripts': [
        ],
    },
    install_requires=(
    ),
)
